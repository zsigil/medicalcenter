import Vue from 'vue';
import Vuetify from 'vuetify/lib';

Vue.use(Vuetify);

export default new Vuetify({
  theme: {
  themes: {
    light: {
      primary: '#081F3E', //rgba(8,31,62,1)
      secondary: '#006CFF', //rgba(0,108,255,1)
      accent: '#8c9eff',
      error: '#b71c1c',
    },
  },
},
});
