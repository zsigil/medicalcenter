import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/rolunk',
    name: 'about',
    component: () => import('../views/About.vue')
  },
  {
    path: '/kapcsolat',
    name: 'contact',
    component: () => import('../views/Contact.vue')
  },
  {
    path: '/arlista',
    name: 'pricing',
    component: () => import('../views/Pricing.vue')
  },
  {
    path: '/orvosok',
    name: 'doctors',
    component: () => import('../views/Doctors.vue')
  },
  {
    path: '*',
    redirect: '/'
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  scrollBehavior (to, from, savedPosition) {
    if (to.hash) {
      return {
        selector: to.hash
        // , offset: { x: 0, y: 10 }
      }
    }else{
      return { x: 0, y: 50 }
    }
  },
  routes
})

export default router
