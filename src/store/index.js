import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    cmsloaded: false,
    szakrendelesek: [
      {
        id: 1,
        elnevezes: "nőgyógyászat",
        leiras: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque lorem diam, efficitur consectetur lacus non, suscipit efficitur purus. Fusce venenatis, ligula suscipit porta sollicitudin, erat ligula commodo arcu, iaculis dictum lacus leo at nisi. Sed tristique odio non elit egestas, ac congue odio vehicula. Cras est justo, sollicitudin quis mattis nec, volutpat vitae enim. Nullam a eros vulputate, auctor odio eu, ultrices nisi. Maecenas laoreet vehicula mi nec semper."
      },
      {
        id:2,
        elnevezes: "bőrgyógyászat",
        leiras: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque lorem diam, efficitur consectetur lacus non, suscipit efficitur purus. Fusce venenatis, ligula suscipit porta sollicitudin, erat ligula commodo arcu, iaculis dictum lacus leo at nisi. Sed tristique odio non elit egestas, ac congue odio vehicula. Cras est justo, sollicitudin quis mattis nec, volutpat vitae enim. Nullam a eros vulputate, auctor odio eu, ultrices nisi. Maecenas laoreet vehicula mi nec semper."
      },
      {
        id:3,
        elnevezes: "gyermekgyógyászat",
        leiras: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque lorem diam, efficitur consectetur lacus non, suscipit efficitur purus. "
      },
      {
        id:4,
        elnevezes: "urológia",
        leiras: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque lorem diam, efficitur consectetur lacus non, suscipit efficitur purus. Fusce venenatis, ligula suscipit porta sollicitudin, erat ligula commodo arcu, iaculis dictum lacus leo at nisi. Sed tristique odio non elit egestas, ac congue odio vehicula. Cras est justo, sollicitudin quis mattis nec, volutpat vitae enim. Nullam a eros vulputate, auctor odio eu, ultrices nisi. Maecenas laoreet vehicula mi nec semper."
      },
      {
        id:5,
        elnevezes: "fogászat",
        leiras: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque lorem diam, efficitur consectetur lacus non, suscipit efficitur purus. Fusce venenatis, ligula suscipit porta sollicitudin, erat ligula commodo arcu, iaculis dictum lacus leo at nisi. Sed tristique odio non elit egestas, ac congue odio vehicula. Cras est justo, sollicitudin quis mattis nec, volutpat vitae enim. Nullam a eros vulputate, auctor odio eu, ultrices nisi. Maecenas laoreet vehicula mi nec semper."
      }
    ],
    orvosok: [
      {
          "id": 1,
          "nev": "dr. Kiss László",
          "doctor_image": {
              "id": 1
          },
          "bemutatkozas": " Give me some of your food give me some of your food give me some of your food meh, i don't want it lounge in doorway and jump off balcony, onto stranger's head or drool intently stare at the same spot, for burrow under covers. A nice warm laptop for me to sit on love you, then bite you. Let me in let me out let me in let me out let me in let me out who broke this door anyway licks your face or cat jumps and falls onto the couch purrs and wakes up in a new dimension filled with kitty litter meow meow yummy there is a bunch of cats hanging around eating catnip for caticus cuteicus x, or demand to be let outside at once, and expect owner to wait for me as i think about it toilet paper attack claws fluff everywhere meow miao french ciao litterbox. Run off table persian cat jump eat fish scratch hunt by meowing loudly at 5am next to human slave food dispenser is good you understand your place in my world. ",
          "szakiranyok": [
              {
                  "id": 2,
                  "status": "published",
                  "created_by": 1,
                  "created_on": "2019-12-12 14:58:15",
                  "orvos": "1",
                  "szakirany": "2"
              },
              {
                  "id": 3,
                  "status": "published",
                  "created_by": 1,
                  "created_on": "2019-12-12 14:58:15",
                  "orvos": "1",
                  "szakirany": "3"
              },
          ]
      },
      {
          "id": 2,
          "nev": "dr. Sándor Sándor",
          "doctor_image": {
              "id": 2
          },
          "bemutatkozas": "Give me some of your food give me some of your food give me some of your food meh, i don't want it lounge in doorway and jump off balcony, onto stranger's head or drool intently stare at the same spot, for burrow under covers. A nice warm laptop for me to sit on love you, then bite you. Let me in let me out let me in let me out let me in let me out who broke this door anyway licks your face or cat jumps and falls onto the couch purrs and wakes up in a new dimension filled with kitty litter meow meow yummy there is a bunch of cats hanging around eating catnip for caticus cuteicus x, or demand to be let outside at once, and expect owner to wait for me as i think about it toilet paper attack claws fluff everywhere meow miao french ciao litterbox. Run off table persian cat jump eat fish scratch hunt by meowing loudly at 5am next to human slave food dispenser is good you understand your place in my world.",
          "szakiranyok": [
              {
                  "id": 1,
                  "status": "published",
                  "created_by": 1,
                  "created_on": "2019-12-12 14:58:15",
                  "orvos": "2",
                  "szakirany": "2"
              },
              {
                  "id": 6,
                  "status": "published",
                  "created_by": 1,
                  "created_on": "2019-12-12 14:58:15",
                  "orvos": "2",
                  "szakirany": "1"
              },
          ]
      },
      {
          "id": 3,
          "nev": "dr. László László",
          "doctor_image": {
              "id": 3
          },
          "bemutatkozas": "Give me some of your food give me some of your food give me some of your food meh, i don't want it lounge in doorway and jump off balcony, onto stranger's head or drool intently stare at the same spot, for burrow under covers. A nice warm laptop for me to sit on love you, then bite you. Let me in let me out let me in let me out let me in let me out who broke this door anyway licks your face or cat jumps and falls onto the couch purrs and wakes up in a new dimension filled with kitty litter meow meow yummy there is a bunch of cats hanging around eating catnip for caticus cuteicus x, or demand to be let outside at once, and expect owner to wait for me as i think about it toilet paper attack claws fluff everywhere meow miao french ciao litterbox. Run off table persian cat jump eat fish scratch hunt by meowing loudly at 5am next to human slave food dispenser is good you understand your place in my world.",
          "szakiranyok": [
              {
                  "id": 4,
                  "status": "published",
                  "created_by": 1,
                  "created_on": "2019-12-12 14:58:15",
                  "orvos": "3",
                  "szakirany": "2"
              },
              {
                  "id": 5,
                  "status": "published",
                  "created_by": 1,
                  "created_on": "2019-12-12 14:58:15",
                  "orvos": "3",
                  "szakirany": "1"
              },
          ]
      },
    ],
    artetelek: [
      {
        "id": 1,
        "status": "published",
        "created_by": 1,
        "created_on": "2019-12-12 14:54:18",
        "elnevezes": "foghúzás",
        "orvosid": 1,
        "ar": 3000
      },
      {
        "id": 2,
        "status": "published",
        "created_by": 1,
        "created_on": "2019-12-12 15:02:18",
        "elnevezes": "oltás beadása",
        "orvosid": 2,
        "ar": 3000
      },
      {
        "id": 3,
        "status": "published",
        "created_by": 1,
        "created_on": "2019-12-12 15:03:33",
        "elnevezes": "kontroll",
        "orvosid": 2,
        "ar": 3000
      }
    ],
  },
  getters:{
    getSzakiranyByID: state => id => {
      return state.szakrendelesek.filter(sz => sz.id == id )[0].elnevezes
    },
    getOrvosByID: state => id =>{
      return state.orvosok.filter(o => o.id == id )[0]
    },
    getSzakiranylistaByOrvosid: state=> id =>{
      let szlista = []
      let orvos = state.orvosok.filter(o => o.id == id )[0]
      for (var i = 0; i < orvos.szakiranyok.length; i++) {
        let szakiranyid = orvos.szakiranyok[i].szakirany
        let szakiranynev = state.szakrendelesek.filter(sz => sz.id == szakiranyid )[0].elnevezes
        szlista.push(szakiranynev)
      }
      return szlista
    },
    getOrvoslistaBySzakiranyid: state=> id =>{
      let orvoslista = []
      for (var i = 0; i < state.orvosok.length; i++) {
        for (var j = 0; j < state.orvosok[i].szakiranyok.length; j++) {
          if (state.orvosok[i].szakiranyok[j].szakirany==id) {
            orvoslista.push(state.orvosok[i])
          }
        }
      }
      return orvoslista
    },
    getOrvosListaABC: state=>{
      return state.orvosok.sort((a,b)=>a.nev<b.nev ? -1:1)
    },
    getArlistaByOrvosid: state=>id=>{
      return state.artetelek.filter(t=>t.orvosid==id)
    }
  },
  mutations: {
    setSzakiranyok(state, payload){
      state.szakrendelesek = payload;
    },
    setOrvosok(state, payload){
      state.orvosok = payload;
    },
    setArtetelek(state,payload){
      state.artetelek = payload;
    },
    setCMSLoaded(state, payload){
      state.cmsloaded = payload;
    }
  },
  actions: {
    connectToServer:({commit, dispatch})=>{
      dispatch('setCMSLoaded', true)

      axios.get('http://51.105.236.88/borokaproba/items/szakirany')
      .then(res=>{
        commit('setSzakiranyok', res.data.data)
      })
      .catch(()=>{
        dispatch('setCMSLoaded', false)
      })

        axios.get('http://51.105.236.88/borokaproba/items/orvos?fields=*.*')
        .then(res=>{
          commit('setOrvosok', res.data.data)
        })
        .catch(()=>{
          dispatch('setCMSLoaded', false)
        })

        axios.get('http://51.105.236.88/borokaproba/items/tetel')
        .then(res=>{
          commit('setArtetelek', res.data.data)
        })
        .catch(()=>{
          dispatch('setCMSLoaded', false)
        })

    },
    setCMSLoaded({commit},payload){
      commit('setCMSLoaded', payload)
    }
  },
  modules: {
  }
})
